package graphics;

import java.awt.BorderLayout;

import javax.swing.JPanel;

import photoAlbum.*;

/**
 * Pannello contenitore dei pannelli necessari per la gestione e la interazione con la fotografia.
 * @author Filippo Fontana
 * @version 1.0
 */
@SuppressWarnings("serial")
public class PhotoContainerPanel extends JPanel
{
	private PhotoFocusPanel photoFocusPanel;
	private PhotoToolBarPanel toolbar;

	/**
	 * Costruttore, esso genera i due pannelli ulteriori necessari alla gestione della
	 * fotografia.
	 * @param album riferimento all'oggetto album.
	 * @param category riferimento all'oggetto album.
	 * @param imagePath percorso dell'immagine da visualizzare.
	 * @param photoIndex indice di tale immagine.
	 * @param containerPanel riferimento all'oggetto pannello contenitore.
	 */
	public PhotoContainerPanel(Album album, Category category, String imagePath, int photoIndex,
							   ContainerPanel containerPanel)
	{
		super();
		
		setLayout(new BorderLayout());
		photoFocusPanel = new PhotoFocusPanel(imagePath);
		toolbar = new PhotoToolBarPanel(containerPanel, this, photoFocusPanel, album, category, photoIndex);
		
		display();
	}
	/**
	 * Questo metodo mostra graficamente i due "sotto-pannelli" creati.
	 */
	public void display()
	{
		add(toolbar, BorderLayout.PAGE_START);
		add(photoFocusPanel, BorderLayout.CENTER);
	}
	/**
	 * Questo metodo si occupa di sostituire le due barre degli strumenti nel caso in cui
	 * l'utente decida di avviare uno slideshow.
	 * @param oldPanel riferimento all'oggetto pannello toolbar della foto.
	 * @param newPanel riferimento all'oggetto pannello toolbar dello slideshow
	 */
	public void fromPhotoToSlideShow(PhotoToolBarPanel oldPanel, SlideShowToolBarPanel newPanel)
	{
		remove(oldPanel);
		add(newPanel, BorderLayout.PAGE_START);
		revalidate();
		repaint();
	}
}