package graphics;

import java.awt.BorderLayout;

import javax.swing.JPanel;

import photoAlbum.*;

/**
 * Pannello contenitore dei pannelli necessari per la gestione e la interazione con la categoria.
 * @author Filippo Fontana
 * @version 1.0
 */
@SuppressWarnings("serial")
public class CategoryContainerPanel extends JPanel
{
	private CategoryFocusPanel categoryFocusPanel;
	private CategoryToolBarPanel categorytoolBar;
	
	/**
	 * Costruttore, esso genera i due pannelli ulteriori necessari alla gestione della
	 * categoria e di tutte le foto presenti in essa.
	 * @param category riferimento all'oggetto categoria.
	 * @param containerPanel riferimento all'oggetto pannello contenitore.
	 * @param album riferimento all'oggetto album.
	 */
	public CategoryContainerPanel(Category category, ContainerPanel containerPanel, Album album)
	{
		super();
		
		setLayout(new BorderLayout());
		category.setContainerPanel(this);
		categoryFocusPanel = new CategoryFocusPanel(album, category, containerPanel);
		categorytoolBar = new CategoryToolBarPanel(category, containerPanel, album);
		
		display();
	}
	/**
	 * Questo metodo mostra graficamente i due "sotto-pannelli" creati.
	 */
	public void display()
	{
		add(categorytoolBar, BorderLayout.PAGE_START);
		add(categoryFocusPanel, BorderLayout.CENTER);
	}
}
