package graphics;

import java.awt.BorderLayout;

import javax.swing.JPanel;

/**
 * Pannello contenitore, volta per volta scambia le diverse "visualizzazioni" richieste
 * dall'utente durante l'interazione con l'app.
 * @author Filippo Fontana
 * @version 1.0
 */
@SuppressWarnings("serial")
public class ContainerPanel extends JPanel
{
	/**
	 * Costruttore, esso setta il layout della finestra.
	 */
	public ContainerPanel()
	{
		super();
		
		setLayout(new BorderLayout());
	}
	/**
	 * Questo metodo aggiunge il pannello di Album Container nella posizione centrale.
	 * @param newPanel pannello da aggiungere.
	 */
	public void addPanel(JPanel newPanel)
	{
		add(newPanel, BorderLayout.CENTER);
	}
	/**
	 * Questo metodo toglie pannello contenitore Album e aggiunge il pannello contenitore Categoria
	 * @param oldPanel pannello contenitore Album
	 * @param newPanel pannello contenitore Categoria
	 */
	public void fromAlbumToCategory(AlbumContainerPanel oldPanel, CategoryContainerPanel newPanel)
	{
		updateDisplay(oldPanel, newPanel);
	}
	/**
	 * Questo metodo toglie pannello contenitore Categoria e aggiunge il pannello contenitore Album
	 * @param oldPanel pannello contenitore Categoria
	 * @param newPanel pannello contenitore Album
	 */
	public void fromCategoryToAlbum(CategoryContainerPanel oldPanel, AlbumContainerPanel newPanel)
	{
		updateDisplay(oldPanel, newPanel);
	}
	/**
	 * Questo metodo toglie pannello contenitore Categoria e aggiunge il pannello contenitore Photo
	 * @param oldPanel pannello contenitore Categoria
	 * @param newPanel pannello contenitore Photo
	 */
	public void fromCategoryToPhoto(CategoryContainerPanel oldPanel, PhotoContainerPanel newPanel)
	{
		updateDisplay(oldPanel, newPanel);
	}
	/**
	 * Questo metodo toglie pannello contenitore Photo e aggiunge il pannello contenitore Categoria
	 * @param oldPanel pannello contenitore Photo
	 * @param newPanel pannello contenitore Categoria
	 */
	public void fromPhotoToCategory(PhotoContainerPanel oldPanel, CategoryContainerPanel newPanel)
	{
		updateDisplay(oldPanel, newPanel);
	}
	/**
	 * Questo metodo svolge le effettive operazioni svolte da tutti i metodi sopra dichiarati.
	 * @param oldPanel pannello da rimuovere.
	 * @param newPanel pannello da aggiungere.
	 */
	public void updateDisplay(JPanel oldPanel, JPanel newPanel)
	{
		remove(oldPanel);
		add(newPanel, BorderLayout.CENTER);
		revalidate();
		repaint();
	}
}
