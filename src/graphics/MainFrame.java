package graphics;

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

import javax.swing.JFrame;
import javax.swing.JScrollPane;

import photoAlbum.Album;

/**
 * <p>MainFrame</p>
 * <p>Finestra che ospita la visualizzazione grafica dell'applicazione.</p>
 * @author Filippo Fontana
 * @version 1.0
 */
@SuppressWarnings("serial")
public class MainFrame extends JFrame
{
	private Album album;
	private WindowListener windowListener;
	
	/**
	 * Costruttore che genera: 
	 * - l'effettivo contenitore di tutti i pannelli interattivi ossia containerPanel;
	 * - le dimensioni della finestra e le operazioni di configurazione necessarie;
	 * @param album riferimento all'oggetto album.
	 * @param albumName nome dell'album, necessario a dare un titolo alla finestra.
	 */
	public MainFrame(Album album, String albumName)
	{
		super(albumName);
		
		this.album = album;
		JScrollPane scrollPanel;
		ContainerPanel containerPanel = new ContainerPanel();
		AlbumContainerPanel albumContainerPanel = new AlbumContainerPanel(album, containerPanel);
		containerPanel.addPanel(albumContainerPanel);
		scrollPanel = new JScrollPane(containerPanel);
		configWindowListener();
		addWindowListener(windowListener);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(1000, 700);
		add(scrollPanel);
		setLocationRelativeTo(null);
		setVisible(true);
	}
	/**
	 * Questo metodo configura correttamente il WindowsListener.
	 * L'unica operazione necessaria e' quella che avviene durante la chiusura
	 * della finestra: il salvataggio dello stato dell'album.
	 */
	public void configWindowListener()
	{
		windowListener = new WindowListener() {
			
			@Override
			public void windowOpened(WindowEvent e) {}
			@Override
			public void windowIconified(WindowEvent e) {}
			
			@Override
			public void windowDeiconified(WindowEvent e) {}
			
			@Override
			public void windowDeactivated(WindowEvent e) {}
			@Override
			public void windowClosing(WindowEvent e)
			{
				saveAlbum();
				System.exit(0);
			}
			@Override
			public void windowClosed(WindowEvent e) {}
			@Override
			public void windowActivated(WindowEvent e) {}
		};
	}
	/**
	 * Questo metodo permette di salvare la versione corrente dell'album
	 * sul file "backUpAlbum.ser".
	 */
	public void saveAlbum()
	{
		File backUpFile = new File("");
		String backUpFilePath = backUpFile.getAbsolutePath() + File.separator + "res"
								+ File.separator + "backUpAlbum.ser";
		try {
			ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(backUpFilePath));
			oos.writeObject(album);
			oos.flush();
			oos.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}