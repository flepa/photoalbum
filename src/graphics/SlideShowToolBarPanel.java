package graphics;

import java.awt.Color;
import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JToolBar;

import photoAlbum.Category;
import toolset.ThreadSlideShow;

/**
 * Pannello che rappresenta la classica barra degli strumenti di una applicazione,
 * in questo caso e' riferita all'oggetto foto (in slide show). E' anche un ascoltatore di eventi.
 * @author Filippo Fontana
 * @version 1.0
 */
@SuppressWarnings("serial")
public class SlideShowToolBarPanel extends JToolBar implements ActionListener
{
	static final private String STOP = "stop";
	private ContainerPanel containerPanel;
    private PhotoContainerPanel photoContainerPanel;
    private Category category;
    private ThreadSlideShow thread;
    
    /**
     * Costruttore, configura la struttura del pannello e lo riempie con i
     * pulsanti appartenenti alla barra degli strumenti.
     * @param containerPanel riferimento all'oggetto pannello contenitore.
     * @param photoContainerPanel riferimento all'oggetto pannello contenitore di foto.
     * @param category riferimento all'oggetto categoria.
     * @param thread riferimento all'oggetto thread slideshow.
     */
    public SlideShowToolBarPanel(ContainerPanel containerPanel, PhotoContainerPanel photoContainerPanel,
    							 Category category, ThreadSlideShow thread)
    {
    	super();
    	
    	setFloatable(false);
    	setRollover(true);
    	setBackground(Color.WHITE);
    	this.containerPanel = containerPanel;
    	this.photoContainerPanel = photoContainerPanel;
    	this.category = category;
    	this.thread = thread;
    	
    	addButtons();
    }
    /**
     * Questo metodo istanzia e aggiunge al pannello i pulsanti necessari.
     */
    public void addButtons()
    {
    	JButton button = null;

        button = makeNavigationButton("stop", STOP);
        add(button);
    }
    /**
     * Questo metodo configura in maniera opportuna i pulsanti.
     * @param imageName icona del pulsante da impostare.
     * @param actionCommand tipo di comando che assegnato al pulsante.
     * @return riferimento a pulsante.
     */
    public JButton makeNavigationButton(String imageName, String actionCommand)
    {
    	Image image = null;
    	File imageFile = new File("");
    	String imageLocation = imageFile.getAbsolutePath() + File.separator + "res" + File.separator +
    						   imageName + ".gif";
    	Toolkit t = Toolkit.getDefaultToolkit();
		MediaTracker m = new MediaTracker(this);
		
		image = t.getImage(imageLocation);
		m.addImage(image, 0);
		try {
			m.waitForAll();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
    	JButton button = new JButton();
    	
    	button.setBackground(Color.WHITE);
        button.setIcon(new ImageIcon(image));
        button.setActionCommand(actionCommand);
        button.setToolTipText("Stop the SlideShow");
        button.addActionListener(this);
        
    	return button;
    }
    /**
     * Questo metodo gestisce gli eventi inerenti a:
     * - richiesta di terminazione dello slide show;
     */
    public void actionPerformed(ActionEvent e)
    {
		thread.setInactive();
		containerPanel.fromPhotoToCategory(photoContainerPanel, category.getContainerPanel());
    }
}
