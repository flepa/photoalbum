package graphics;

import java.awt.*;
import javax.swing.*;

/**
 * Pannello che gestisce la visualizzazione della foto ad alta risoluzione e le interazioni con essa.
 * @author Filippo Fontana
 * @version 1.0
 */
@SuppressWarnings("serial")
public class PhotoFocusPanel extends JPanel
{
	private Image image;
	
	/**
	 * Costruttore, configura la struttura del pannello e crea i presupposti per
	 * disegnarci sopra la foto.
	 * @param imagePath percorso dell'immagine.
	 */
	public PhotoFocusPanel(String imagePath)
	{
		super();
		
		setBackground(Color.WHITE);
		image = getImage(imagePath);
	}
	@Override
	public void paint(Graphics g)
	{
		super.paint(g);
		
		g.drawImage(image, 0, 0, getWidth(), getHeight(), null);
	}
	/**
	 * Metodo che aggiorna il contenuto del pannello: viene mostrata una nuova immagine.
	 * @param imagePath percorso della nuova immagine.
	 */
	public void updatePanel(String imagePath)
	{
		image = getImage(imagePath);
		repaintPanel();
	}
	/*
	 * Questo metodo richiede di selezionare il nome della categoria in cui spostare la foto
	 * attuale.
	 */
	public String askDestination(Object[] categories)
	{
		String destination = (String)JOptionPane.showInputDialog(this, "Insert the destination category",
																 "Move", JOptionPane.INFORMATION_MESSAGE,
																 null, categories, categories[0]);
		
		return destination;
	}
	/**
	 * Questo metodo segnala che non puo' essere compiuto uno spostamento.
	 */
	public void moveError()
	{
		JOptionPane.showMessageDialog(this, "It's not possible to move this photo!",
									  "Error in: move", JOptionPane.ERROR_MESSAGE);
	}
	/**
	 * Questo metodo segnala che non puo' essere compiuto uno slide show.
	 */
	public void slideShowError()
	{
		JOptionPane.showMessageDialog(this, "It's not possible to start a Slide Show!",
									  "Error in: slideshow", JOptionPane.ERROR_MESSAGE);
	}
	/**
	 * Metodo che crea un oggetto immagine da mostrare graficamente.
	 * @param imagePath percorso dell'immagine
	 * @return riferimento all'oggetto immagine.
	 */
	public Image getImage(String imagePath)
	{
		Toolkit t = Toolkit.getDefaultToolkit();
		MediaTracker m = new MediaTracker(this);
		
		image = t.getImage(imagePath);
		m.addImage(image, 0);
		try {
			m.waitForAll();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		return image;
	}
	/**
	 * Questo metodo aggiorna il pannello.
	 */
	public void repaintPanel()
	{
		revalidate();
		repaint();
	}
}
