package graphics;

import java.awt.BorderLayout;

import javax.swing.JPanel;

import photoAlbum.Album;

/**
 * Pannello contenitore dei pannelli necessari per la gestione e la interazione con l'album.
 * @author Filippo Fontana
 * @version 1.0
 */
@SuppressWarnings("serial")
public class AlbumContainerPanel extends JPanel
{
	private AlbumFocusPanel albumPanel;
	private AlbumToolBarPanel toolBar;
	
	/**
	 * Costruttore, esso genera i due pannelli ulteriori necessari alla gestione dell'album
	 * e di tutte le categorie presenti in esso.
	 * @param album riferimento all'oggetto album.
	 * @param containerPanel riferimento all'oggetto pannello contenitore.
	 */
	public AlbumContainerPanel(Album album, ContainerPanel containerPanel)
	{
		super();
		
		setLayout(new BorderLayout());
		album.setContainerPanel(this);
		albumPanel = new AlbumFocusPanel(album, containerPanel);
		toolBar = new AlbumToolBarPanel(album, albumPanel);
		
		display();
	}
	/**
	 * Questo metodo mostra graficamente i due "sotto-pannelli" creati.
	 */
	public void display()
	{
		add(toolBar, BorderLayout.PAGE_START);
		add(albumPanel, BorderLayout.CENTER);
	}
}