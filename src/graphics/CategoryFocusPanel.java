package graphics;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

import listeners.ListenerToPhoto;
import photoAlbum.*;

import java.awt.*;
import java.util.Vector;

/**
 * Pannello che gestisce la visualizzazione delle foto e le interazioni con la categoria.
 * @author Filippo Fontana
 * @version 1.0
 */
@SuppressWarnings("serial")
public class CategoryFocusPanel extends JPanel
{
	//astrazione delle anteprime delle foto:
	private Vector<DefaultButton> btns;
	private Album album;
	private Category category;
	private ContainerPanel containerPanel;
	//tiene traccia dell'indice dell'ultima categoria inserita:
	private int latestIndex;
	
	/**
	 * Costruttore, configura la struttura del pannello e lo riempie con l'elenco delle
	 * foto presenti nella categoria (se esso � stato ripristinato da file).
	 * @param album riferimento all'oggetto album.
	 * @param category riferimento all'oggetto categoria.
	 * @param containerPanel riferimento all'oggetto pannello contenitore.
	 */
	public CategoryFocusPanel(Album album, Category category, ContainerPanel containerPanel)
	{
		super();
		
		setLayout(new GridLayout(0, 4, 1, 10));
		setBackground(Color.WHITE);
		setBorder(new EmptyBorder(20, 0, 20, 0));
		this.album = album;
		this.category = category;
		this.category.setFocusPanel(this);
		this.containerPanel = containerPanel;
		btns = new Vector<DefaultButton>();
		latestIndex = this.category.getSize();
		
		fillPanel();
	}
	/**
	 * Questo metodo si occupa di riempire il pannello di una categoria ripristinata da file.
	 */
	public void fillPanel()
	{
		for (int i = 0; i < category.getSize(); i++) {
			String imagePath = category.getImage(i);
			addBtnImage(i, imagePath);
		}
		repaintPanel();
	}
	/**
	 * Questo metodo aggiunge una foto alla categoria e si occupa anche di aggiungere un nuovo
	 * pulsante al pannello.
	 * @param imagePath percorso dell'immagine.
	 */
	public void addToPanel(String imagePath)
	{
		//operazioni categoria:
		category.addPhoto(imagePath);
		//operazioni pannello:
		addBtnImage(latestIndex, imagePath);
		latestIndex++;
		repaintPanel();
	}
	/**
	 * Questo metodo aggiunge un pulsante (con relativa copertina) al pannello categoria.
	 * @param index indice del pulsante.
	 * @param imagePath percorso dell'immagine.
	 */
	public void addBtnImage(int index, String imagePath)
	{
		Image image = null;
		//e' la prima foto? Allora devo aggiornare la copertina della categoria.
		if (index == 0 && !category.isBlocked())
			(album.getFocusPanel()).updateCover(category);

		Toolkit t = Toolkit.getDefaultToolkit();
		MediaTracker m = new MediaTracker(this);
		
		image = t.getImage(imagePath);
		m.addImage(image, 0);
		try {
			m.waitForAll();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		Image scaledImage = image.getScaledInstance( 100, 100,  java.awt.Image.SCALE_SMOOTH );
		DefaultButton b = new DefaultButton("");

		b.setActionCommand(index + "");
		b.setIcon(new ImageIcon(scaledImage));
		b.addActionListener(new ListenerToPhoto(containerPanel, album, category));
		btns.add(b);
		add(b);
	}
	/**
	 * Questo metodo rimuove un'immagine (insieme al suo pulsante) dalla categoria
	 * e dal pannello.
	 * @param index indice della categoria e del pulsante.
	 */
	public void removeImage(int index)
	{		
		DefaultButton b = btns.get(index);
		
		//se il bottone eliminato non � l'ultimo: riparo la navigazione
		if (index < latestIndex)
			fixActionCmd(index);
		// operazioni categoria:
		category.removePhoto(index);
		//e' la prima foto? Allora devo aggiornare la copertina della categoria.
		if (index == 0 && !category.isBlocked())
			(album.getFocusPanel()).updateCover(category);
		// operazioni pannello:
		remove(b);
		btns.remove(index);
		latestIndex--;
		repaintPanel();
	}
	/**
	 * Questo metodo aggiorna gli indici di navigazione (sia dei pulsanti che delle immagini)
	 * dopo l'eliminazione di un pulsante.
	 * @param index indice del pulsante eliminato.
	 */
	public void fixActionCmd(int index)
	{
		for (int i = index + 1; i < btns.size(); i++) {
			DefaultButton b = btns.get(i);
			
			b.setActionCommand("" + (i - 1));
		}
	}
	/**
	 * Questo metodo richiede la password per modificare i metadati di una categoria protetta e
	 * verifica la correttezza della stessa.
	 * @param passwd la password della categoria protetta.
	 * @return vero o falso a seconda del confronto.
	 */
	public boolean askPasswd(String passwd)
	{	
		JPasswordField pf = new JPasswordField();
		pf.requestFocus();
		int okCtrl = JOptionPane.showConfirmDialog(this, pf, "Enter Password",
													JOptionPane.OK_CANCEL_OPTION, 
													JOptionPane.PLAIN_MESSAGE);
		
		if (okCtrl == JOptionPane.OK_OPTION) {
		  String password = new String(pf.getPassword());
		  
		  if (password.equals(passwd))
			  return true;
		  else
			  return false;
		}
		
		return false;
	}
	/**
	 * Questo metodo segnala l'inserimento di una password errata.
	 */
	public void wrongPasswd()
	{
		JOptionPane.showMessageDialog(this, "Wrong password!",
										"Error", JOptionPane.ERROR_MESSAGE);
	}
	/**
	 * Questo metodo richiede di selezionare il nome della categoria in cui spostare tutte le foto
	 * presenti nella categoria attuale.
	 * @param categories elenco delle categorie disponibili.
	 * @return riferimento a stringa contenente il nome della categoria selezionata.
	 */
	public String askDestination(Object[] categories)
	{
		String destination = (String)JOptionPane.showInputDialog(this, "Insert the destination category",
																 "Merge", JOptionPane.INFORMATION_MESSAGE,
																 null, categories, categories[0]);
		
		return destination;
	}
	/**
	 * Questo metodo segnala che non puo' essere compiuto uno spostamento.
	 */
	public void mergeError()
	{
		JOptionPane.showMessageDialog(this, "It's impossible to merge this category!",
									  "Error in: merge", JOptionPane.ERROR_MESSAGE);
	}
	/**
	 * Questo metodo segnala il corretto avvenimento dell'operazione.
	 */
	public void mergeCompleted ()
	{
		JOptionPane.showMessageDialog(this, "Merge completed!",
									  "Merge", JOptionPane.INFORMATION_MESSAGE);
	}
	/**
	 * Questo metodo aggiorna il pannello.
	 */
	public void repaintPanel()
	{
		revalidate();
		repaint();
	}
	/**
	 * Questo metodo aggiorna il riferimento alla categoria quando questa viene protetta
	 * da una password.
	 * @param category nuovo riferimento ad una categoria protetta.
	 */
	public void updateCategory(Category category)
	{
		this.category = category;
	}
}
