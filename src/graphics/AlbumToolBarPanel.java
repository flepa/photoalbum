package graphics;

import java.awt.Color;
import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JToolBar;

import photoAlbum.Album;

/**
 * Pannello che rappresenta la classica barra degli strumenti di una applicazione,
 * in questo caso e' riferita all'oggetto album. E' anche un ascoltatore di eventi.
 * @author Filippo Fontana
 * @version 1.0
 */
@SuppressWarnings("serial")
public class AlbumToolBarPanel extends JToolBar implements ActionListener
{
    static final private String ADD = "add";
    static final private String INFO = "info";
    private Album album;
    private AlbumFocusPanel albumFocusPanel;
    
    /**
     * Costruttore, configura la struttura del pannello e lo riempie con i
     * pulsanti appartenenti alla barra degli strumenti.
     * @param album riferimento all'oggetto album.
     * @param albumFocusPanel riferimento all'oggetto pannello "focus" dell'album.
     */
    public AlbumToolBarPanel(Album album, AlbumFocusPanel albumFocusPanel)
    {
    	super();
    	
    	setFloatable(false);
    	setRollover(true);
    	setBackground(Color.WHITE);
    	this.album = album;
    	this.albumFocusPanel = albumFocusPanel;
    	
    	addButtons();
    }
    /**
     * Questo metodo istanzia e aggiunge al pannello i pulsanti necessari.
     */
    public void addButtons()
    {
    	JButton button = null;

        button = makeNavigationButton("add", ADD);
        add(button);
        addSeparator();
        button = makeNavigationButton("info", INFO);
        add(button);
    }
    /**
     * Questo metodo configura in maniera opportuna i pulsanti.
     * @param imageName icona del pulsante da impostare.
     * @param actionCommand tipo di comando che assegnato al pulsante.
     * @return riferimento a pulsante.
     */
    public JButton makeNavigationButton(String imageName, String actionCommand)
    {
    	Image image = null;
    	File imageFile = new File("");
    	String imageLocation = imageFile.getAbsolutePath() + File.separator + "res" + File.separator +
    						 imageName + ".gif";
    	Toolkit t = Toolkit.getDefaultToolkit();
		MediaTracker m = new MediaTracker(this);
		
		image = t.getImage(imageLocation);
		m.addImage(image, 0);
		try {
			m.waitForAll();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
    	JButton button = new JButton();
    	
    	button.setBackground(Color.WHITE);
        button.setActionCommand(actionCommand);
        button.addActionListener(this);
        button.setIcon(new ImageIcon(image));
        if (actionCommand.equals(ADD))
        	 button.setToolTipText("Create a new category");
        else
        	 button.setToolTipText("About album");
        
        return button;
    }
    /**
     * Questo metodo gestisce gli eventi inerenti a:
     * - aggiunta di una nuova categoria;
     * - richiesta informazioni riguardo all'album;
     */
    public void actionPerformed(ActionEvent e)
    {
    	if (e.getActionCommand().equals(ADD)) {
    		JTextField name = new JTextField(25);
    		JTextField description = new JTextField(25);
    		Object[] fields = {"Name: ", name, "Description: ", description};
    		int option = JOptionPane.showConfirmDialog(albumFocusPanel, 
    												   fields, "Add Category",
    												   JOptionPane.OK_CANCEL_OPTION);
    		if (option == JOptionPane.OK_OPTION) {
    			albumFocusPanel.addToPanel(name.getText(), description.getText());
    		}
    	} else if (e.getActionCommand().equals(INFO)) {
    		JOptionPane.showMessageDialog(albumFocusPanel, album.getName() + "\n"+ album.getDate(),
    									  "About", JOptionPane.INFORMATION_MESSAGE);
    	}
    }
}
