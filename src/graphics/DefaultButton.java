package graphics;

import java.awt.Color;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.SwingConstants;

/**
 * Versione personalizzata di un JButton.
 * @author Filippo Fontana
 * @version 1.0
 */
@SuppressWarnings("serial")
public class DefaultButton extends JButton
{
	private Font font;
	
	/**
	 * Costruttore, configura il pulsante in maniera opportuna.
	 * @param name nome da attribuire al pulsante.
	 */
	public DefaultButton(String name)
	{
		super(name);
		
		font = new Font("Arial", Font.BOLD, 15);
		setFont(font);
		setBackground(Color.WHITE);
		setBorder(null);
		setHorizontalTextPosition(SwingConstants.CENTER);
		setVerticalTextPosition(SwingConstants.BOTTOM);
	}
}
