package graphics;

import java.awt.Color;
import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.time.LocalDate;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.JToolBar;

import listeners.ListenerMerge;
import listeners.ListenerPhotoAdd;
import listeners.ListenerToAlbum;
import photoAlbum.*;

/**
 * Pannello che rappresenta la classica barra degli strumenti di una applicazione,
 * in questo caso e' riferita all'oggetto categoria. E' anche un ascoltatore di eventi.
 * @author Filippo Fontana
 * @version 1.0
 */
@SuppressWarnings("serial")
public class CategoryToolBarPanel extends JToolBar implements ActionListener
{
    static final private String ADD = "add";
    static final private String PASSWD = "passwd";
    static final private String EDIT = "edit";
    static final private String UP = "up";
    static final private String INFO = "info";
    static final private String DEL = "delete";
    static final private String MERGE = "merge";
    private Category category;
    private ContainerPanel containerPanel;
    private Album album;
    
    /**
     * Costruttore, configura la struttura del pannello e lo riempie con i
     * pulsanti appartenenti alla barra degli strumenti.
     * @param category riferimento all'oggetto categoria. 
     * @param containerPanel riferimento all'oggetto pannello contenitore.
     * @param album riferimento all'oggetto album.
     */
    public CategoryToolBarPanel(Category category, ContainerPanel containerPanel, Album album)
    {
    	super();
    	
    	setFloatable(false);
    	setRollover(true);
    	setBackground(Color.WHITE);
    	this.category = category;
    	this.containerPanel = containerPanel;
    	this.album = album;
    	addButtons();
    }
    /**
     * Questo metodo istanzia e aggiunge al pannello i pulsanti necessari.
     */
    public void addButtons()
    {
    	JButton button = null;

        button = makeNavigationButton("add", ADD);
        add(button);
        addSeparator();
        button = makeNavigationButton("passwd", PASSWD);
        add(button);
        addSeparator();
        button = makeNavigationButton("edit", EDIT);
        add(button);
        addSeparator();
        button = makeNavigationButton("Up24", UP);
        add(button);
        addSeparator();
        button = makeNavigationButton("info", INFO);
        add(button);
        addSeparator();
        button = makeNavigationButton("delete", DEL);
        add(button);
        addSeparator();
        button = makeNavigationButton("merge", MERGE);
        add(button);
    }
    /**
     * Questo metodo configura in maniera opportuna i pulsanti.
     * @param imageName icona del pulsante da impostare.
     * @param actionCommand tipo di comando che assegnato al pulsante.
     * @return riferimento a pulsante.
     */
    public JButton makeNavigationButton(String imageName, String actionCommand)
    {
    	Image image = null;
    	File imageFile = new File("");
    	String imageLocation = imageFile.getAbsolutePath() + File.separator + "res" + File.separator +
    						   imageName + ".gif";
    	Toolkit t = Toolkit.getDefaultToolkit();
		MediaTracker m = new MediaTracker(this);
		
		image = t.getImage(imageLocation);
		m.addImage(image, 0);
		try {
			m.waitForAll();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
    	JButton button = new JButton();
    	
    	button.setBackground(Color.WHITE);
        button.setIcon(new ImageIcon(image));
        button.setActionCommand(actionCommand);
        if (actionCommand.equals(ADD)) {
        	button.setToolTipText("Add an image");
        	button.addActionListener(new ListenerPhotoAdd(category.getFocusPanel()));
        } else if (actionCommand.equals(PASSWD)) {
        	button.setToolTipText("Set a password");
        	button.addActionListener(this);
        } else if (actionCommand.equals(EDIT)) {
        	button.setToolTipText("Edit category data");
        	button.addActionListener(this);
        } else if (actionCommand.equals(UP)) {
        	button.setToolTipText("Go back to the album");
        	button.addActionListener(new ListenerToAlbum(category.getContainerPanel(),
        												 containerPanel,
					 									 album.getContainerPanel()));
        } else if (actionCommand.equals(INFO)) {
        	button.setToolTipText("About category");
        	button.addActionListener(this);
        } else if (actionCommand.equals(DEL)) {
        	button.setToolTipText("Remove category");
        	button.addActionListener(this);
        } else if (actionCommand.equals(MERGE)) {
        	button.setToolTipText("Combine two categories");
        	button.addActionListener(new ListenerMerge(album, category, containerPanel));
        }
        	
        return button;
    }
    /**
     * Questo metodo gestisce gli eventi inerenti a:
     * - aggiunta di una password alla categoria;
     * - modifica dati di una categoria;
     * - richiesta informazioni riguardo alla categoria;
     */
    public void actionPerformed(ActionEvent e)
    {
		if (e.getActionCommand().equals(PASSWD)) {
	    	JPasswordField passwd1 = new JPasswordField();
	    	JPasswordField passwd2 = new JPasswordField();
			Object[] passwds = {"New Password: ", passwd1, "Retype Password: ", passwd2};
			int okCtrl = JOptionPane.showConfirmDialog(category.getFocusPanel(), passwds,
													   "Edit Category",
													   JOptionPane.OK_CANCEL_OPTION,
													   JOptionPane.PLAIN_MESSAGE);
			if (okCtrl == JOptionPane.OK_OPTION) {
				String passwd = new String(passwd1.getPassword());
				String checkPasswd = new String(passwd2.getPassword());
				
				if (passwd.equals(checkPasswd)) {
					category = backUpRestore(passwd);
	
					//test
					(category.getFocusPanel()).updateCategory(category);
					//fine
					(album.getFocusPanel()).updateCategory(category.getIndex(), category);
					JOptionPane.showMessageDialog(category.getFocusPanel(), 
												  "password entered correctly!");
					containerPanel.fromCategoryToAlbum(category.getContainerPanel(),
													   album.getContainerPanel());
				} else {
					JOptionPane.showMessageDialog(category.getFocusPanel(),
												  "Passwords doesn't match, try again!",
												  "Password mismatch", JOptionPane.ERROR_MESSAGE);
				}
			}
		} else if (e.getActionCommand().equals(EDIT)) {
    		JTextField name = new JTextField(25);
    		JTextField description = new JTextField(25);
    		Object[] fields = {"New Name: ", name, "New Description: ", description};
    		int option = JOptionPane.showConfirmDialog(category.getFocusPanel(), fields,
    												   "Edit Category", JOptionPane.OK_CANCEL_OPTION);
    		
    		if (option == JOptionPane.OK_OPTION) {
    			String newName = name.getText();
    			String newDesc = description.getText();
    			
    			category.modifyCategory(newName, newDesc);
    			//bisogna aggiornare l'etichetta del pulsante categoria:
    			(album.getFocusPanel()).updateBtnCategory(category.getIndex());
    		}
    	} else if (e.getActionCommand().equals(INFO)) {
    		JOptionPane.showMessageDialog(category.getFocusPanel(), 
    									  category.getName() + "\n" +
    									  category.getDate() + "\n" +
    									  category.getDescription(),
    									  "About", JOptionPane.INFORMATION_MESSAGE);
    	} else if (e.getActionCommand().equals(DEL)) {
    		(album.getFocusPanel()).removeCategory(category.getIndex());    		
    		JOptionPane.showMessageDialog(category.getFocusPanel(), "Category deleted correctly!",
										  "Category deleted", JOptionPane.ERROR_MESSAGE);
    		containerPanel.fromCategoryToAlbum(category.getContainerPanel(), album.getContainerPanel());
    	}
    }
    /**
     * Questo metodo crea una nuova categoria protetta ricavando i dati dalla categoria corrente.
     * @param passwd password impostata.
     * @return riferimento ad un oggetto categoria protetta.
     */
    public ProtectedCategory backUpRestore(String passwd)
    {
    	String name = category.getName();
    	String description = category.getDescription();
    	LocalDate date = category.getDate();
    	Vector<String> images = category.getImages();
    	CategoryContainerPanel backUpContainer = category.getContainerPanel();
    	CategoryFocusPanel backUpFocus = category.getFocusPanel();
    	int catIndex = category.getIndex();
    	ProtectedCategory passwdCategory = new ProtectedCategory(name, date, description, passwd);
    	
    	passwdCategory.setImages(images);
    	passwdCategory.setContainerPanel(backUpContainer);
    	passwdCategory.setFocusPanel(backUpFocus);
    	passwdCategory.setIndex(catIndex);
    	(album.getFocusPanel()).updateCover(passwdCategory);
    	
    	return passwdCategory;
    }
}