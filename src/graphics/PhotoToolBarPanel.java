package graphics;

import java.awt.Color;
import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JToolBar;

import listeners.ListenerFromPhoto;
import listeners.ListenerMove;
import listeners.ListenerSlideShow;
import photoAlbum.*;

/**
 * Pannello che rappresenta la classica barra degli strumenti di una applicazione,
 * in questo caso e' riferita all'oggetto foto. E' anche un ascoltatore di eventi.
 * @author Filippo Fontana
 * @version 1.0
 */
@SuppressWarnings("serial")
public class PhotoToolBarPanel extends JToolBar implements ActionListener
{
	static final private String PREVIOUS = "previous";
    static final private String UP = "up";
    static final private String NEXT = "next";
    static final private String SLIDE = "slide";
    static final private String MOVE = "move";
    static final private String DEL = "delete";
    private ContainerPanel containerPanel;
    private PhotoContainerPanel photoContainerPanel;
    private PhotoFocusPanel photoFocusPanel;
    private Album album;
    private Category category;
    // per navigare tra le immagini:
    private int navIndex;
    
    /**
     * Costruttore, configura la struttura del pannello e lo riempie con i
     * pulsanti appartenenti alla barra degli strumenti.
     * @param containerPanel riferimento all'oggetto pannello contenitore.
     * @param photoContainerPanel riferimento all'oggetto pannello contenitore di foto.
     * @param photoFocusPanel riferimento all'oggetto pannello focus di foto.
     * @param album riferimento all'oggetto album.
     * @param category riferimento all'oggetto categoria.
     * @param navIndex riferimento all'indice di navigazione dell'immagine corrente.
     */
    public PhotoToolBarPanel(ContainerPanel containerPanel, PhotoContainerPanel photoContainerPanel,
						PhotoFocusPanel photoFocusPanel, Album album, Category category, int navIndex)
    {
    	super();
    	
    	setFloatable(false);
    	setRollover(true);
    	setBackground(Color.WHITE);
    	this.containerPanel = containerPanel;
    	this.photoContainerPanel = photoContainerPanel;
    	this.photoFocusPanel = photoFocusPanel;
    	this.album = album;
    	this.category = category;
    	this.navIndex = navIndex;
    	
    	addButtons();
    }
    /**
     * Questo metodo istanzia e aggiunge al pannello i pulsanti necessari.
     */
    public void addButtons()
    {
    	JButton button = null;

        button = makeNavigationButton("Back24", PREVIOUS);
        add(button);
        addSeparator();
        button = makeNavigationButton("Up24", UP);
        add(button);
        addSeparator();
        button = makeNavigationButton("Forward24", NEXT);
        add(button);
        addSeparator();
        button = makeNavigationButton("slideShow", SLIDE);
        add(button);
        addSeparator();
        button = makeNavigationButton("delete", DEL);
        add(button);
        addSeparator();
        button = makeNavigationButton("move", MOVE);
        add(button);
    }
    /**
     * Questo metodo configura in maniera opportuna i pulsanti.
     * @param imageName icona del pulsante da impostare.
     * @param actionCommand tipo di comando che assegnato al pulsante.
     * @return riferimento a pulsante.
     */
    public JButton makeNavigationButton(String imageName, String actionCommand)
    {
    	Image image = null;
    	File imageFile = new File("");
    	String imageLocation = imageFile.getAbsolutePath() + File.separator + "res" + File.separator +
    						   imageName + ".gif";
    	Toolkit t = Toolkit.getDefaultToolkit();
		MediaTracker m = new MediaTracker(this);
		
		image = t.getImage(imageLocation);
		m.addImage(image, 0);
		try {
			m.waitForAll();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
    	JButton button = new JButton();
    	
    	button.setBackground(Color.WHITE);
        button.setIcon(new ImageIcon(image));
        button.setActionCommand(actionCommand);
        if (actionCommand.equals(PREVIOUS)) {
        	button.setToolTipText("Previous photo");
        	button.addActionListener(this);
        } else if (actionCommand.equals(UP)) {
        	button.setToolTipText("Go back to the category");
        	button.addActionListener(new ListenerFromPhoto(containerPanel, category.getContainerPanel(),
        							 					   photoContainerPanel));
        } else if (actionCommand.equals(NEXT)) {
        	button.setToolTipText("Next photo");
        	button.addActionListener(this);
        } else if (actionCommand.equals(SLIDE)) {
        	button.setToolTipText("Start a slideshow");
        	button.addActionListener(new ListenerSlideShow(category, photoContainerPanel, photoFocusPanel,
        												   this, containerPanel));	
        } else if (actionCommand.equals(DEL)) {
        	button.setToolTipText("Remove this photo");
        	button.addActionListener(this);
        }
        else if (actionCommand.equals(MOVE)) {
        	button.setToolTipText("Move this photo");
        	button.addActionListener(new ListenerMove(album, category, navIndex, photoContainerPanel,
        											  photoFocusPanel, containerPanel));
        }
        
        return button;
    }
    /**
     * Questo metodo gestisce gli eventi inerenti a:
     * - richiesta di visualizzazione della foto precedente;
     * - richiesta di visualizzazione della foto successiva;
     * - richiesta di eliminazione di una foto dalla categoria;
     */
    public void actionPerformed(ActionEvent e)
    {
    	if (e.getActionCommand().equals(PREVIOUS)) {
    		if (navIndex > 0) {
    			navIndex--;
    			photoFocusPanel.updatePanel(category.getImage(navIndex));
    		} else {
    			JOptionPane.showMessageDialog(photoFocusPanel, "This is the first photo!",
											  "Navigation error", JOptionPane.ERROR_MESSAGE);
    		}
    	} else if (e.getActionCommand().equals(NEXT)) {
    		if (navIndex < (category.getSize() - 1)) {
    			navIndex++;
    			photoFocusPanel.updatePanel(category.getImage(navIndex));
    		} else {
    			JOptionPane.showMessageDialog(photoFocusPanel, "This is the last photo!",
											  "Navigation error", JOptionPane.ERROR_MESSAGE);
    		}
    	} else if (e.getActionCommand().equals(DEL)) {    		
    		(category.getFocusPanel()).removeImage(navIndex);
    		JOptionPane.showMessageDialog(photoFocusPanel, "Photo deleted correctly!",
										  "Photo deleted", JOptionPane.ERROR_MESSAGE);
    		containerPanel.fromPhotoToCategory(photoContainerPanel, category.getContainerPanel());
    	}
    }
}