package graphics;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.Toolkit;
import java.io.File;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.border.EmptyBorder;

import listeners.ListenerToCategory;
import photoAlbum.Album;
import photoAlbum.Category;
import photoAlbum.ProtectedCategory;

/**
 * Pannello che gestisce la visualizzazione delle categorie e le interazioni con l'album.
 * @author Filippo Fontana
 * @version 1.0
 */
@SuppressWarnings("serial")
public class AlbumFocusPanel extends JPanel
{
	//astrazione delle anteprime delle categorie:
	private Vector<DefaultButton> btns;
	private Album album;
	private ContainerPanel containerPanel;
	//tiene traccia dell'indice dell'ultima categoria inserita:
	private int latestIndex;
	
	/**
	 * Costruttore, configura la struttura del pannello e lo riempie con l'elenco delle
	 * categorie presenti nell'album (se esso � stato ripristinato da file).
	 * @param album riferimento all'oggetto album.
	 * @param containerPanel riferimento all'oggetto containerPanel.
	 */
	public AlbumFocusPanel(Album album, ContainerPanel containerPanel)
	{
		super();
		
		setLayout(new GridLayout(0, 4, 1, 10));
		setBackground(Color.WHITE);
		//da' un po' di padding al contenuto del pannello.
		setBorder(new EmptyBorder(20, 0, 20, 0));
		this.album = album;
		this.album.setFocusPanel(this);
		this.containerPanel = containerPanel;
		btns = new Vector<DefaultButton>();
		latestIndex = this.album.getSize();
		
		fillPanel();
	}
	/**
	 * Questo metodo si occupa di riempire il pannello di un album ripristinato da file.
	 */
	public void fillPanel()
	{	
		for (int i = 0; i < album.getSize(); i++) {
			String categoryName = (album.getCategory(i)).getName();
			addBtnCategory(i, categoryName);
		}
		repaintPanel();
	}
	/**
	 * Questo metodo aggiunge una categoria all'album e si occupa anche di aggiungere un nuovo
	 * pulsante al pannello.
	 * @param categoryName nome della categoria.
	 * @param description descrizione della categoria.
	 */
	public void addToPanel(String categoryName, String description)
	{
		// operazioni album:
		album.addCategory(categoryName, description);
		// per la navigazione:
		(album.getCategory(latestIndex)).setIndex(latestIndex);
		// operazioni pannello:
		addBtnCategory(latestIndex, categoryName);
		latestIndex++;
		repaintPanel();
	}
	/**
	 * Questo metodo aggiunge un pulsante (con relativa copertina) al pannello album.
	 * @param index indice del pulsante.
	 * @param categoryName nome del pulsante.
	 */
	public void addBtnCategory(int index, String categoryName)
	{
		Image scaledCover = null;
		
		if ((album.getCategory(index)).getSize() == 0 || (album.getCategory(index)).isBlocked())
			scaledCover = defaultCover();
		else
			scaledCover = getCover((album.getCategory(index)).getImage(0));
		
		DefaultButton b = new DefaultButton((album.getCategory(index)).getName());

		b.setIcon(new ImageIcon(scaledCover));
		b.setActionCommand(index + "");
		b.addActionListener(new ListenerToCategory(containerPanel, album));
		btns.add(b);
		add(b);
	}
	/**
	 * Questo metodo modifica l'etichetta del pulsante associato ad una categoria.
	 * Questo avviene quando vengono modificati i metadati alla categoria.
	 * @param index indice posseduto dalla categoria e dal pulsante che la rappresenta.
	 */
	public void updateBtnCategory(int index)
	{
		DefaultButton b = btns.get(index);
		
		b.setText((album.getCategory(index)).getName());
	}
	/**
	 * Questo metodo si occupa di aggiornare una categoria, a cui viene impostata una
	 * password, attraverso l'apposito metodo della classe Album.
	 * @param index indice categoria.
	 * @param passwdCategory nuovo riferimento della categoria (ora protetta). 
	 */
	public void updateCategory(int index, Category passwdCategory)
	{
		album.replaceCategory(index, passwdCategory);
	}
	/**
	 * Questo metodo rimuove un oggetto categoria (insieme al suo pulsante) dall'album
	 * e dal pannello.
	 * @param index indice della categoria e del pulsante.
	 */
	public void removeCategory(int index)
	{
		DefaultButton b = btns.get(index);
		
		//se il bottone eliminato non � l'ultimo: riparo la navigazione
		if (index < latestIndex)
			fixActionCmd(index);
		//operazioni album:
		(album.getCategory(index)).removeAll();
		album.removeCategory(index);
		//operazioni pannello:
		remove(b);
		btns.remove(index);
		latestIndex--;
		repaintPanel();
	}
	/**
	 * Questo metodo ritorna un'immagine (di default) ritagliata per essere usata come copertina da un pulsante.
	 * @return riferimento a tale immagine.
	 */
	public Image defaultCover()
	{
		Image image = null;
		File imageFile = new File("");
		String imagePath = imageFile.getAbsolutePath() + File.separator + "res" + File.separator +
						   "sf6.jpg";
		Toolkit t = Toolkit.getDefaultToolkit();
		MediaTracker m = new MediaTracker(this);
		
		image = t.getImage(imagePath);
		m.addImage(image, 0);
		try {
			m.waitForAll();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		return image.getScaledInstance(100, 100,  java.awt.Image.SCALE_SMOOTH);
	}
	/**
	 * Questo metodo ritorna un'immagine ritagliata, viene ottenuta dalla locazione passata. 
	 * @param coverPath percorso da cui ricavare l'immagine.
	 * @return riferimento a tale immagine.
	 */
	public Image getCover(String coverPath)
	{
		Image cover = null;
		Toolkit t = Toolkit.getDefaultToolkit();
		MediaTracker m = new MediaTracker(this);
		
		cover = t.getImage(coverPath);
		m.addImage(cover, 0);
		try {
			m.waitForAll();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		return cover.getScaledInstance(100, 100,  java.awt.Image.SCALE_SMOOTH);
	}
	/**
	 * Questo metodo aggiorna la copertina di una categoria, lo fa in tre casi:
	 * - viene aggiunta una foto ad una categoria vuota;
	 * - viene rimossa la foto che rappresenta la copertina di una categoria;
	 * - viene settata una password alla categoria;
	 * @param category riferimento alla categoria.
	 */
	public void updateCover(Category category)
	{
		Image cover = null;
		DefaultButton b = null;
		if (category.getSize() == 0 || category.isBlocked()) {
			cover = defaultCover();
		} else {
			Toolkit t = Toolkit.getDefaultToolkit();
			MediaTracker m = new MediaTracker(this);
			cover = t.getImage(category.getImage(0));
			m.addImage(cover, 0);
			try {
				m.waitForAll();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		b = btns.get(category.getIndex());
		b.setIcon(new ImageIcon(cover.getScaledInstance(100, 100,  java.awt.Image.SCALE_SMOOTH)));
	}
	/**
	 * Questo metodo aggiorna gli indici di navigazione (sia dei pulsanti che delle categorie)
	 * dopo l'eliminazione di un pulsante.
	 * @param index indice del pulsante eliminato.
	 */
	public void fixActionCmd(int index)
	{
		for (int i = index + 1; i < btns.size(); i++) {
			DefaultButton b = btns.get(i);
			
			b.setActionCommand("" + (i - 1));
			(album.getCategory(i)).setIndex(i - 1);
		}
	}
	/**
	 * Questo metodo richiede una password per accedere alla categoria protetta
	 * e la confronta con la password settata nella categoria.
	 * @param category riferimento alla categoria.
	 * @return vero o falso a seconda dell'esito del confronto.
	 */
	public boolean askPasswd(Category category)
	{	
		JPasswordField pf = new JPasswordField();
		pf.requestFocus();
		int okCtrl = JOptionPane.showConfirmDialog(this, pf, "Enter Password",
													JOptionPane.OK_CANCEL_OPTION, 
													JOptionPane.PLAIN_MESSAGE);
		
		if (okCtrl == JOptionPane.OK_OPTION) {
		  String password = new String(pf.getPassword());
		  
		  if (password.equals(((ProtectedCategory) category).getPasswd()))
			  return true;
		  else
			  return false;
		}
		
		return false;
	}
	/**
	 * Questo metodo segnala l'inserimento di una password errata.
	 */
	public void wrongPasswd()
	{
		JOptionPane.showMessageDialog(this, "Wrong password!",
										"Error", JOptionPane.ERROR_MESSAGE);
	}
	/**
	 * Questo metodo aggiorna il pannello.
	 */
	public void repaintPanel()
	{
		revalidate();
		repaint();
	}
}