package listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import graphics.ContainerPanel;
import graphics.PhotoContainerPanel;
import graphics.PhotoFocusPanel;
import graphics.PhotoToolBarPanel;
import graphics.SlideShowToolBarPanel;
import photoAlbum.Category;
import toolset.ThreadSlideShow;

/**
 * Ascoltatore che gestisce l'evento in cui l'utente decide di avviare uno
 * slideshow in una categoria.
 * @author Filippo Fontana
 * @version 1.0
 */
public class ListenerSlideShow implements ActionListener
{
	private Category category;
	private PhotoContainerPanel photoContainerPanel;
	private PhotoFocusPanel photoFocusPanel;
	private PhotoToolBarPanel photoToolBarPanel;
	private ThreadSlideShow thread;
	private ContainerPanel containerPanel;
	
	/**
	 * Costruttore, inizializza gli oggetti necessari all'ascoltatore per operare
	 * correttamente.
	 * @param category riferimento all'oggetto categoria.
	 * @param photoContainerPanel riferimento all'oggetto pannello contenitore di foto.
	 * @param photoFocusPanel riferimento all'oggetto pannello focus di foto.
	 * @param photoToolBarPanel riferimento all'oggetto pannello toolbar di foto.
	 * @param photoIndex indice della foto di partenza.
	 * @param containerPanel riferimento all'oggetto pannello contenitore.
	 */
	public ListenerSlideShow(Category category, PhotoContainerPanel photoContainerPanel, 
							 PhotoFocusPanel photoFocusPanel, PhotoToolBarPanel photoToolBarPanel,
							 ContainerPanel containerPanel)
	{
		this.category = category;
		this.photoContainerPanel = photoContainerPanel;
		this.photoFocusPanel = photoFocusPanel;
		this.photoToolBarPanel = photoToolBarPanel;
		this.containerPanel = containerPanel;
	}
	/**
	 * Questo metodo gestisce lo slide show di foto.
	 */
	public void actionPerformed(ActionEvent e)
	{
		if (e.getActionCommand().equals("slide")) {
			if (category.getSize() > 1) {
				thread = new ThreadSlideShow(2000, photoFocusPanel, category);
				
				photoContainerPanel.fromPhotoToSlideShow(photoToolBarPanel, 
														 new SlideShowToolBarPanel(containerPanel,
																				   photoContainerPanel,
																				   category, 
																			  	   thread));
				thread.start();
			} else {
				photoFocusPanel.slideShowError();
			}
		}
	}
}
