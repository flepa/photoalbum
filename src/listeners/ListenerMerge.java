package listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import graphics.ContainerPanel;
import photoAlbum.*;

/**
 * Ascoltatore che gestisce l'evento in cui l'utente decide di spostare una collezione
 * di fotografie in una categoria diversa.
 * @author Filippo Fontana
 * @version 1.0
 */
public class ListenerMerge implements ActionListener
{
	private Album album;
	private Category category;
	private Object[] categories;
	private ContainerPanel containerPanel;
	
	/**
	 * Costruttore, inizializza gli oggetti necessari all'ascoltatore per operare
	 * correttamente.
	 * @param album riferimento all'oggetto album.
	 * @param category riferimento all'oggetto categoria.
	 * @param containerPanel riferimento all'oggetto pannello contenitore.
	 */
	public ListenerMerge(Album album, Category category, ContainerPanel containerPanel)
	{
		this.album = album;
		this.category = category;
		this.containerPanel = containerPanel;
	}
	/**
	 * Questo metodo gestisce lo spostamento di tutte le foto nella categoria di destinazione.
	 */
	public void actionPerformed(ActionEvent e)
	{
		if (album.getSize() > 1 && category.getSize() > 0) {
			//categories avra' un elemento in meno:
			categories = new Object[album.getSize() - 1];
			Category destCategory = null;
			//indice indipendente:
			int index = 0; 
			// obiettivo: creare una lista di categorie selezionabili come destinazione:
			for (int i = 0; i < album.getSize(); i++) {
				if (i != category.getIndex()) {
					categories[index] = (album.getCategory(i)).getName();
					index++;
				}
			}
			// se l'utente annulla l'operazione viene ritornato null.
			String catName = (category.getFocusPanel()).askDestination(categories);
			
			if (catName != null) { 
				//bisogna ottenere l'indice della categoria:
				int catIndex = getIndex(catName);

				destCategory = album.getCategory(catIndex);
				merge(category, destCategory);
				(category.getFocusPanel()).mergeCompleted();
				containerPanel.fromCategoryToAlbum(category.getContainerPanel(), album.getContainerPanel());
			}
		} else {
			(category.getFocusPanel()).mergeError();
		}
	}
	/**
	 * Questo metodo ritorna l'indice di posizione della categoria corrispondente al parametro passato.
	 * @param catName nome della categoria di cui si vuole l'indice
	 * @return indice cercato.
	 */
	public int getIndex(String catName)
	{
		//modo grezzo per interrompere il for:
		for (int i = 0; i < album.getSize(); i++) {
			if (((album.getCategory(i)).getName()).equals(catName))
				return i;
		}
		
		return 0;
	}
	/**
	 * Questo metodo si occupa di svolgere l'effettiva operazione di spostamento.
	 * @param source riferimento alla categoria sorgente.
	 * @param destination riferimento alla categoria destinazione.
	 */
	public void merge(Category source, Category destination)
	{
		// necessario per non perdere dinamicamente la dimensione della categoria:
		int sourceSize = source.getSize();
		
		for (int i = 0; i < sourceSize; i++) {
			if (destination.getFocusPanel() != null) {
				(destination.getFocusPanel()).addToPanel(source.getImage(i));
			} else {
				destination.addPhoto(source.getImage(i));
				//e' la prima foto? Allora devo aggiornare la copertina della categoria.
				if (i == 0 && !destination.isBlocked())
					album.getFocusPanel().updateCover(destination);
			}
		}
		// ripulisco la sorgente:
		source.removeAll();
		(album.getFocusPanel()).removeCategory(category.getIndex());
	}
}
