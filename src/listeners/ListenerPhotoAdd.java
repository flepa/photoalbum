package listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JFileChooser;

import graphics.CategoryFocusPanel;
import toolset.ImageFilter;
import toolset.ImageView;

/**
 * Ascoltatore che gestisce l'evento in cui l'utente decide di aggiungere una fotografia
 * in una categoria.
 * @author Filippo Fontana
 * @version 1.0
 */
public class ListenerPhotoAdd implements ActionListener
{
	private JFileChooser fc;
	private CategoryFocusPanel categoryFocusPanel;
	
	/**
	 * Costruttore, inizializza gli oggetti necessari all'ascoltatore per operare
	 * correttamente.
	 * @param categoryFocusPanel riferimento all'oggetto pannello focus di categoria.
	 */
	public ListenerPhotoAdd(CategoryFocusPanel categoryFocusPanel)
	{
		this.categoryFocusPanel = categoryFocusPanel;
	}
	/**
	 * Questo metodo gestisce l'aggiunta di una foto.
	 */
	public void actionPerformed(ActionEvent e)
	{
		if (e.getActionCommand().equals("add")) {
			if (fc == null) {
	    		fc = new JFileChooser();
	
	    	    //Aggiunge un filtro file personalizzato e disabilita il filtro predefinito (Accetta tutto):
	            fc.addChoosableFileFilter(new ImageFilter());
	            fc.setAcceptAllFileFilterUsed(false);
	    	    //Aggiunge le icone per i tipi di file:
	            fc.setFileView(new ImageView());
	    	}
			int returnVal = fc.showDialog(categoryFocusPanel, "Add to the category");
	
	        if (returnVal == JFileChooser.APPROVE_OPTION) {
	            File fileImage = fc.getSelectedFile();
	            
	            categoryFocusPanel.addToPanel(fileImage.getPath());
	        }
		}
	}
}
