package listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import graphics.AlbumContainerPanel;
import graphics.CategoryContainerPanel;
import graphics.ContainerPanel;

/**
 * Ascoltatore che gestisce l'evento in cui l'utente decide di passare dal pannello
 * focus di una categoria al pannello contenitore di album.
 * @author Filippo Fontana
 * @version 1.0
 */
public class ListenerToAlbum implements ActionListener
{
	private CategoryContainerPanel categoryContainerPanel;
	private ContainerPanel containerPanel;
	private AlbumContainerPanel albumContainerPanel;
	
	/**
	 * Costruttore, inizializza gli oggetti necessari all'ascoltatore per operare
	 * correttamente.
	 * @param categoryContainerPanel riferimento all'oggetto pannello contenitore di categoria.
	 * @param containerPanel riferimento all'oggetto pannello contenitore.
	 * @param albumContainerPanel riferimento all'oggetto pannello contenitore di album.
	 */
	public ListenerToAlbum(CategoryContainerPanel categoryContainerPanel, ContainerPanel containerPanel, AlbumContainerPanel albumContainerPanel)
	{
		this.categoryContainerPanel = categoryContainerPanel;
		this.containerPanel = containerPanel;
		this.albumContainerPanel = albumContainerPanel;
	}
	/**
	 * Questo metodo gestisce il passaggio tra i due pannelli.
	 */
	public void actionPerformed(ActionEvent e)
	{
		if (e.getActionCommand().equals("up"))
			sendPanels();
	}
	/**
	 * Questo metodo permette l'effettivo scambio dei due pannelli.
	 */
	public void sendPanels()
	{
		containerPanel.fromCategoryToAlbum(categoryContainerPanel, albumContainerPanel);
	}
}
