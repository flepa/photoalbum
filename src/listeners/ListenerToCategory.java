package listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import graphics.CategoryContainerPanel;
import graphics.ContainerPanel;
import photoAlbum.*;

/**
 * Ascoltatore che gestisce l'evento in cui l'utente decide di passare dal pannello
 * focus di album ad un pannello contenitore di una categoria.
 * @author Filippo Fontana
 * @version 1.0
 */
public class ListenerToCategory implements ActionListener
{
	private ContainerPanel containerPanel;
	private Album album;
	
	/**
	 * Costruttore, inizializza gli oggetti necessari all'ascoltatore per operare
	 * correttamente.
	 * @param containerPanel riferimento all'oggetto pannello contenitore.
	 * @param album riferimento all'oggetto album.
	 */
	public ListenerToCategory(ContainerPanel containerPanel, Album album)
	{
		this.containerPanel = containerPanel;
		this.album = album;
	}
	/**
	 * Questo metodo gestisce il passaggio tra i due pannelli.
	 * NOTA: l'action command contiene l'indice della categoria che si vuole aprire.
	 */
	public void actionPerformed(ActionEvent e)
	{
		Integer index = Integer.parseInt(e.getActionCommand());
		Category category = album.getCategory(index);
		CategoryContainerPanel categoryContainerPanel = null;

		if (category.getContainerPanel() == null) {
			categoryContainerPanel = new CategoryContainerPanel(category, containerPanel, album);
		} else {
			categoryContainerPanel = category.getContainerPanel();
		}
		if (category.isBlocked()) {
			if ((album.getFocusPanel()).askPasswd(category))
				sendPanels(categoryContainerPanel);
			else
				(album.getFocusPanel()).wrongPasswd();
		} else {
			sendPanels(categoryContainerPanel);
		}
	}
	/**
	 * Questo metodo permette l'effettivo scambio dei due pannelli.
	 * @param toCategory riferimento all'oggetto pannello contenitore di categoria.
	 */
	public void sendPanels(CategoryContainerPanel toCategory)
	{
		containerPanel.fromAlbumToCategory(album.getContainerPanel(), toCategory);
	}
}
