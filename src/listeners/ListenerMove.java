package listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import graphics.ContainerPanel;
import graphics.PhotoContainerPanel;
import graphics.PhotoFocusPanel;
import photoAlbum.*;

/**
 * Ascoltatore che gestisce l'evento in cui l'utente decide di spostare una fotografia
 * in una categoria diversa.
 * @author Filippo Fontana
 * @version 1.0
 */
public class ListenerMove implements ActionListener
{
	private Album album;
	private Category category;
	private int photoIndex;
	private PhotoContainerPanel photoContainerPanel;
	private PhotoFocusPanel photoFocusPanel;
	private ContainerPanel containerPanel;
	private Object[] categories;
	
	/**
	 * Costruttore, inizializza gli oggetti necessari all'ascoltatore per operare
	 * correttamente.
	 * @param album riferimento all'oggetto album.
	 * @param category riferimento all'oggetto categoria.
	 * @param photoIndex indice della foto da spostare.
	 * @param photoContainerPanel riferimento all'oggetto pannello contenitore di foto.
	 * @param photoFocusPanel riferimento all'oggetto pannello focus di foto.
	 * @param containerPanel riferimento all'oggetto pannello contenitore.
	 */
	public ListenerMove(Album album, Category category, int photoIndex, 
						PhotoContainerPanel photoContainerPanel, PhotoFocusPanel photoFocusPanel,
						ContainerPanel containerPanel)
	{
		this.album = album;
		this.category = category;
		this.photoIndex = photoIndex;
		this.photoContainerPanel = photoContainerPanel;
		this.photoFocusPanel = photoFocusPanel;
		this.containerPanel = containerPanel;
	}
	/**
	 * Questo metodo gestisce lo spostamento di una foto nella categoria di destinazione.
	 */
	public void actionPerformed(ActionEvent e)
	{
		if (e.getActionCommand().equals("move")) {
			if (album.getSize() > 1 && category.getSize() > 0) {
				//categories avra' un elemento in meno:
				categories = new Object[album.getSize() - 1];
				Category destCategory = null;
				//indice indipendente:
				int index = 0;
				// obiettivo: creare una lista di categorie selezionabili come destinazione:
				for (int i = 0; i < album.getSize(); i++) {
					if (i != category.getIndex()) {
						categories[index] = (album.getCategory(i)).getName();
						index++;
					}
				}
				// se l'utente annulla l'operazione viene ritornato null.
				String catName = photoFocusPanel.askDestination(categories);
				
				if (catName != null) { 
					//bisogna ottenere l'indice della categoria:
					int catIndex = getIndex(catName);
	
					destCategory = album.getCategory(catIndex);
					move(category, destCategory);
					containerPanel.fromPhotoToCategory(photoContainerPanel, category.getContainerPanel());
				}
			} else {
				photoFocusPanel.moveError();
			}
		}
	}
	/**
	 * Questo metodo ritorna l'indice di posizione della categoria corrispondente al parametro passato.
	 * @param catName nome della categoria di cui si vuole l'indice
	 * @return indice cercato.
	 */
	public int getIndex(String catName)
	{
		//modo grezzo per interrompere il for:
		for (int i = 0; i < album.getSize(); i++) {
			if (((album.getCategory(i)).getName()).equals(catName))
				return i;
		}
		
		return 0;
	}
	/**
	 * Questo metodo si occupa di svolgere l'effettiva operazione di spostamento.
	 * @param source riferimento alla categoria sorgente.
	 * @param destination riferimento alla categoria destinazione.
	 */
	public void move(Category source, Category destination)
	{
		if (destination.getFocusPanel() != null) {
			(destination.getFocusPanel()).addToPanel(source.getImage(photoIndex));
		} else {
			destination.addPhoto(source.getImage(photoIndex));
			//e' la prima foto? Allora devo aggiornare la copertina della categoria.
			if (photoIndex == 0 && !destination.isBlocked())
				(album.getFocusPanel()).updateCover(destination);
		}
		(source.getFocusPanel()).removeImage(photoIndex);
	}
}
