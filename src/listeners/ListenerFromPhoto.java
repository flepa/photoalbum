package listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import graphics.CategoryContainerPanel;
import graphics.ContainerPanel;
import graphics.PhotoContainerPanel;

/**
 * Ascoltatore che gestisce l'evento in cui l'utente decide di passare dal pannello
 * focus di una foto al pannello contenitore di una categoria.
 * @author Filippo Fontana
 * @version 1.0
 */
public class ListenerFromPhoto implements ActionListener
{
	private CategoryContainerPanel categoryContainerPanel;
	private PhotoContainerPanel photoContainerPanel;
	private ContainerPanel containerPanel;
	
	/**
	 * Costruttore, inizializza gli oggetti necessari all'ascoltatore per operare
	 * correttamente.
	 * @param containerPanel riferimento all'oggetto pannello contenitore.
	 * @param categoryContainerPanel riferimento all'oggetto pannello contenitore di categoria.
	 * @param photoContainerPanel riferimento all'oggetto pannello contenitore di foto.
	 */
	public ListenerFromPhoto(ContainerPanel containerPanel, CategoryContainerPanel categoryContainerPanel,
							 PhotoContainerPanel photoContainerPanel)
	{
		this.containerPanel = containerPanel;
		this.categoryContainerPanel = categoryContainerPanel;
		this.photoContainerPanel = photoContainerPanel;
	}
	/**
	 * Questo metodo gestisce il passaggio tra i due pannelli.
	 */
	public void actionPerformed(ActionEvent e)
	{
		if (e.getActionCommand().equals("up"))
			sendPanels();
	}
	/**
	 * Questo metodo permette l'effettivo scambio dei due pannelli.
	 */
	public void sendPanels()
	{
		containerPanel.fromPhotoToCategory(photoContainerPanel, categoryContainerPanel);
	}
}
