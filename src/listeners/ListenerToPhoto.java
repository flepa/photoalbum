package listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import graphics.ContainerPanel;
import graphics.PhotoContainerPanel;
import photoAlbum.*;

/**
 * Ascoltatore che gestisce l'evento in cui l'utente decide di passare dal pannello
 * focus di album ad un pannello contenitore di una categoria.
 * @author Filippo Fontana
 * @version 1.0
 */
public class ListenerToPhoto implements ActionListener
{
	private ContainerPanel containerPanel;
	private Album album;
	private Category category;
	
	/**
	 * Costruttore, inizializza gli oggetti necessari all'ascoltatore per operare
	 * correttamente.
	 * @param containerPanel riferimento all'oggetto pannello contenitore.
	 * @param album riferimento all'oggetto album.
	 * @param category riferimento all'oggetto categoria.
	 */
	public ListenerToPhoto(ContainerPanel containerPanel, Album album, Category category)
	{
		this.containerPanel = containerPanel;
		this.album = album;
		this.category = category;
	}
	/**
	 * Questo metodo gestisce il passaggio tra i due pannelli.
	 * NOTA: l'action command contiene l'indice della foto che si vuole aprire.
	 */
	public void actionPerformed(ActionEvent e)
	{
		Integer photoIndex = Integer.parseInt(e.getActionCommand());
		String imagePath = category.getImage(photoIndex);
		PhotoContainerPanel photoContainerPanel = new PhotoContainerPanel(album, category, imagePath,
																		  photoIndex, containerPanel);
		sendPanels(photoContainerPanel);
	}
	/**
	 * Questo metodo permette l'effettivo scambio dei due pannelli.
	 * @param toPhoto riferimento all'oggetto pannello contenitore della foto.
	 */
	public void sendPanels(PhotoContainerPanel toPhoto)
	{
		containerPanel.fromCategoryToPhoto(category.getContainerPanel(), toPhoto);
	}
}
