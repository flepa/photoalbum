package photoAlbum;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Vector;

import graphics.AlbumContainerPanel;
import graphics.AlbumFocusPanel;

/**
 * L'album rappresenta l'intera collezione di categorie.
 * @author Filippo Fontana
 * @version 1.0
 */
@SuppressWarnings("serial")
public class Album implements Serializable
{
	private String name;
	private LocalDate creationDate;
	private Vector<Category> categories;
	//Variabili che rappresentano i pannelli su cui fa riferimento l'album:
	transient private AlbumContainerPanel albumContainerPanel;
	transient private AlbumFocusPanel albumFocusPanel;
	
	/**
	 * Costruttore, inizializza l'album.
	 * @param name nome dell'album.
	 * @param creationDate data di creazione.
	 */
	public Album(String name, LocalDate creationDate)
	{
		this.name = name;
		this.creationDate = creationDate;
		categories = new Vector<Category>();
	}
	public void setContainerPanel(AlbumContainerPanel albumContainerPanel)
	{
		this.albumContainerPanel = albumContainerPanel;
	}
	public AlbumContainerPanel getContainerPanel()
	{
		return albumContainerPanel;
	}
	public void setFocusPanel(AlbumFocusPanel albumFocusPanel)
	{
		this.albumFocusPanel = albumFocusPanel;
	}
	public AlbumFocusPanel getFocusPanel()
	{
		return albumFocusPanel;
	}
	public String getName()
	{
		return name;
	}
	public LocalDate getDate()
	{
		return creationDate;
	}
	/**
	 * Questo metodo aggiunge una categoria all'album.
	 * @param categoryName nome categoria
	 * @param description descrizione categoria
	 */
	public void addCategory(String categoryName, String description)
	{
		LocalDate date = LocalDate.now();
		categories.add(new Category(categoryName, date, description));
	}
	public void removeCategory(int index)
	{
		categories.remove(index);
	}
	/**
	 * Questo metodo sostituisce una categoria non protetta esistente con la sua versione
	 * protetta.
	 * @param index indice della categoria all'interno della struttura dati album.
	 * @param category riferimento all'oggetto categoria specifico.
	 */
	public void replaceCategory(int index, Category category)
	{
		categories.setElementAt(category, index);
	}
	public Vector<Category> getCategories()
	{
		return categories;
	}
	/**
	 * Questo metodo ritorna la categoria presente nella struttura dati album che corrisponde
	 * all'indice passato come parametro.
	 * @param index indice della categoria da ottenere.
	 * @return ritorna l'oggetto categoria cercato.
	 */
	public Category getCategory(int index)
	{
		return categories.get(index);
	}
	/**
	 * Questo metodo ritorna la dimensione attuale dell'album.
	 * @return valore intero che rappresenta la dimensione.
	 */
	public int getSize()
	{
		return categories.size();
	}
}