package photoAlbum;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Vector;

import graphics.CategoryContainerPanel;
import graphics.CategoryFocusPanel;

/**
 * La categoria rappresenta una collezione di foto.
 * @author Filippo Fontana
 * @version 1.0
 */
@SuppressWarnings("serial")
public class Category implements Serializable
{
	protected String name;
	protected LocalDate creationDate;
	protected String description;
	protected Vector<String> images;
	// stato di protezione della categoria:
	protected boolean block;
	//indice della categoria nella struttura dati dell'album.
	protected int catIndex; 
	//variabili che rappresentano i pannelli su cui fa riferimento la categoria:
	transient protected CategoryContainerPanel categoryContainerPanel;
	transient protected CategoryFocusPanel categoryFocusPanel;
	
	/**
	 * Costruttore, inizializza la categoria.
	 * @param name nome della categoria.
	 * @param creationDate data di creazione.
	 * @param description descrizione.
	 */
	public Category(String name, LocalDate creationDate, String description)
	{
		this.name = name;
		this.creationDate = creationDate;
		this.description = description;
		images = new Vector<String>();
		block = false;
	}
	public void setContainerPanel(CategoryContainerPanel categoryContainerPanel)
	{
		this.categoryContainerPanel = categoryContainerPanel;
	}
	public void setFocusPanel(CategoryFocusPanel categoryFocusPanel)
	{
		this.categoryFocusPanel = categoryFocusPanel;
	}
	public CategoryContainerPanel getContainerPanel()
	{
		return categoryContainerPanel;
	}
	public CategoryFocusPanel getFocusPanel()
	{
		return categoryFocusPanel;
	}
	/**
	 * Questo metodo modifica i dati della categoria.
	 * @param name nuovo nome.
	 * @param description nuova descrizione.
	 */
	public void modifyCategory(String name, String description)
	{
		this.name = name;
		this.description = description;
	}
	public void addPhoto(String imagePath)
	{
		images.add(imagePath);
	}
	public void removePhoto(int index)
	{
		images.remove(index);
	}
	public String getName()
	{
		return name;
	}
	public LocalDate getDate()
	{
		return creationDate;
	}
	public String getDescription()
	{
		return description;
	}
	/**
	 * Questo metodo e' necessario nel momento in cui viene istanziata una categoria protetta.
	 * @param images riferimento ad un contenitore di immagini.
	 */
	public void setImages(Vector<String> images)
	{
		this.images = images;
	}
	/**
	 * Questo metodo e' necessario nel momento in cui viene istanziata una categoria protetta.
	 * @return riferimento ad un contenitore di immagini.
	 */
	public Vector<String> getImages()
	{
		return images;
	}
	/**
	 * Questo metodo ritorna la dimensione attuale della categoria.
	 * @return valore intero che rappresenta la dimensione.
	 */
	public int getSize()
	{
		return images.size();
	}
	/**
	 * Questo metodo ritorna il percorso dell'immagine presente nella struttura dati categoria
	 * che corrisponde all'indice passato come parametro.
	 * @param index indice dell'immagine da ottenere.
	 * @return ritorna l'oggetto immagine cercato.
	 */
	public String getImage(int index)
	{
		return images.get(index);
	}
	/**
	 * Questo metodo controlla se la categoria � protetta.
	 * @return valore vero o falso.
	 */
	public boolean isBlocked()
	{
		return block;
	}
	/**
	 * Questo metodo imposta l'indice della posizione in cui la categoria e' salvata nella
	 * struttura dati album.
	 * @param index indice della posizione.
	 */
	public void setIndex(int index)
	{
		catIndex = index;
	}
	public int getIndex()
	{
		return catIndex;
	}
	/**
	 * Questo metodo ripulisce la struttura dati della categoria quando essa viene eliminata.
	 */
	public void removeAll()
	{
		images.clear();
	}
}