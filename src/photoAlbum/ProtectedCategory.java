package photoAlbum;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * Una categoria protetta da password.
 * @author Filippo Fontana
 * @version 1.0
 */
@SuppressWarnings("serial")
public class ProtectedCategory extends Category implements Serializable
{
	private String passwd;
	
	/**
	 * Costruttore, richiama il costruttore padre ed imposta password e stato (bloccato)
	 * della nuova categoria.
	 * @param name nome della categoria.
	 * @param creationDate data di creazione.
	 * @param description descrizione della categoria.
	 * @param passwd password della categoria.
	 */
	public ProtectedCategory(String name, LocalDate creationDate, String description, String passwd)
	{
		super(name, creationDate, description);
		
		super.block = true;
		this.passwd = passwd;
	}
	/**
	 * Metodo che prima di apportare modifiche ai metadati di una categoria protetta richiede la
	 * password impostata nella categoria, per questioni di sicurezza.
	 */
	@Override
	public void modifyCategory(String name, String description)
	{
		if ((super.getFocusPanel()).askPasswd(getPasswd()))
			super.modifyCategory(name, description);
		else
			(super.getFocusPanel()).wrongPasswd();
	}
	public String getPasswd()
	{
		return passwd;
	}
}
