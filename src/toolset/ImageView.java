package toolset;

import java.io.File;
import javax.swing.*;
import javax.swing.filechooser.*;

/**
 * Strumento necessario ad assegnare delle icone ai file analizzati dal file chooser.
 * @author Filippo Fontana
 * @version 1.0
 */
public class ImageView extends FileView {
	protected static File iconFile = new File("");
	protected static String iconPath = File.separator + "res" + File.separator;
	protected ImageIcon jpgIcon = FileTool.createImageIcon(iconPath + "jpgIcon.gif");
	protected ImageIcon gifIcon = FileTool.createImageIcon(iconPath + "gifIcon.gif");
	protected ImageIcon tiffIcon = FileTool.createImageIcon(iconPath + "tiffIcon.gif");
	protected ImageIcon pngIcon = FileTool.createImageIcon(iconPath + "pngIcon.png");

	/**
	 * gestito direttamente dal L&F di FileView.
	 */
    public String getName(File f) {
        return null;
    }
    /**
	 * gestito direttamente dal L&F di FileView.
	 */
    public String getDescription(File f) {
        return null;
    }
    /**
	 * gestito direttamente dal L&F di FileView.
	 */
    public Boolean isTraversable(File f) {
        return null;
    }

    /**
     * Questo metodo ritorna il tipo del file analizzato.
     */
    public String getTypeDescription(File f) {
        String extension = FileTool.getExtension(f);
        String type = null;

        if (extension != null) {
            if (extension.equals(FileTool.jpeg) ||
                extension.equals(FileTool.jpg)) {
                type = "JPEG Image";
            } else if (extension.equals(FileTool.gif)){
                type = "GIF Image";
            } else if (extension.equals(FileTool.tiff) ||
                       extension.equals(FileTool.tif)) {
                type = "TIFF Image";
            } else if (extension.equals(FileTool.png)){
                type = "PNG Image";
            }
        }
        return type;
    }
    /**
     * Questo metodo ritorna un riferimento di tipo Icon a seconda del file analizzato.
     */
    public Icon getIcon(File f) {
        String extension = FileTool.getExtension(f);
        Icon icon = null;

        if (extension != null) {
            if (extension.equals(FileTool.jpeg) ||
                extension.equals(FileTool.jpg)) {
                icon = jpgIcon;
            } else if (extension.equals(FileTool.gif)) {
                icon = gifIcon;
            } else if (extension.equals(FileTool.tiff) ||
                       extension.equals(FileTool.tif)) {
                icon = tiffIcon;
            } else if (extension.equals(FileTool.png)) {
                icon = pngIcon;
            }
        }
        return icon;
    }
}
