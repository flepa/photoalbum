package toolset;

import java.awt.Image;
import java.io.File;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

/**
 * Strumento necessario per il riconoscimento dell'estensione dei file analizzati
 * dal file chooser.
 * @author Filippo Fontana
 * @version 1.0
 */
public class FileTool
{
    protected final static String jpeg = "jpeg";
    protected final static String jpg = "jpg";
    protected final static String gif = "gif";
    protected final static String tiff = "tiff";
    protected final static String tif = "tif";
    protected final static String png = "png";

    /**
     * Questo metodo ricava l'estensione di un file.
     * @param f riferimento ad un oggetto file.
     * @return ritorna una stringa contenente l'estensione.
     */
    public static String getExtension(File f) {
        String ext = null;
        String s = f.getName();
        int i = s.lastIndexOf('.');

        if (i > 0 && i < s.length() - 1)
            ext = s.substring(i+1).toLowerCase();
  
        return ext;
    }

    /**
     * Questo metodo ritorna un ImageIcon, o null se il percorso non viene riconosciuto.
     * @param path percorso dell'icona.
     * @return riferimento ad un oggetto ImageIcon.
     */
    protected static ImageIcon createImageIcon(String path)
    {
    	File f = new File("");
        String imgLocation = f.getAbsolutePath() + path;
        Image image;
        
    	try {
			image = ImageIO.read(new File(imgLocation));
		} catch (Exception e) {
			System.err.println("Errore in FileTool: File not found!");
			
			return null;
		}
    	
    	return new ImageIcon(image);
    }
}
