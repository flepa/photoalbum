package toolset;

import graphics.PhotoFocusPanel;
import photoAlbum.Category;

/**
 * Strumento necessario a garantire l'alternarsi delle immagini in uno slide show.
 * @author Filippo Fontana
 * @version 1.0
 */
public class ThreadSlideShow extends Thread
{
	private final int period;
	private boolean active;
	private PhotoFocusPanel photoFocusPanel;
	private Category category;
	
	/**
	 * Costruttore, inizializza gli oggetti necessari all'ascoltatore per operare
	 * correttamente.
	 * @param period tempo di "sleep()" del thread.
	 * @param photoFocusPanel riferimento all'oggetto pannello focus di foto.
	 * @param category riferimento all'oggetto categoria
	 * @param photoIndex indice della foto di partenza.
	 */
	public ThreadSlideShow(int period, PhotoFocusPanel photoFocusPanel, Category category) // int photoIndex)
	{
		this.period = period;
		this.photoFocusPanel = photoFocusPanel;
		this.category = category;
		this.active = true;
	}
	/**
	 * Questo metodo permette la corretta esecuzione dello slideshow.
	 */
	@Override
	public void run()
	{
		int i = 0;
		
		while (active) {
			try {
				Thread.sleep(period);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			//1� metodo:
			/*// ottengo un numero tra 0 e getSize - 1
			int randIndex = random.nextInt(category.getSize());
			// gestisco casi limite:
			if (randIndex == photoIndex) {
				if (randIndex == category.getSize())
					randIndex--;
				else if (randIndex == 0)
					randIndex++;
			}
			photoFocusPanel.updatePanel(category.getImage(randIndex));
			// l'ultimo indice diventa quello attuale:
			photoIndex = randIndex;*/
			//2� metodo:
			photoFocusPanel.updatePanel(category.getImage(i % category.getSize()));
			i++;
		}
	}
	/**
	 * Questo metodo serve per bloccare l'esecuzione del thread e farlo "morire".
	 */
	public void setInactive()
	{
		active = false;
	}
}
