package toolset;

import java.io.File;
import javax.swing.filechooser.*;

/**
 * Strumento necessario per filtrare i file visualizzati dal file chooser, 
 * questo filtro serve per consentire la visualizzazione delle directory e delle sole immagini.
 * @author Filippo Fontana
 * @version 1.0
 */
public class ImageFilter extends FileFilter
{
    /**
     * Questo metodo indica quali tipi di file puo' visualizzare il file chooser, ossia:
     * tutte le cartelle e tutti i file gif, jpg, tiff, o png.
     * @param f riferimento ad un oggetto file
     * @return vero o falso
     */
    public boolean accept(File f)
    {
        if (f.isDirectory())
            return true;

        String extension = FileTool.getExtension(f);
        if (extension != null) {
            if (extension.equals(FileTool.tiff) ||
                extension.equals(FileTool.tif) ||
                extension.equals(FileTool.gif) ||
                extension.equals(FileTool.jpeg) ||
                extension.equals(FileTool.jpg) ||
                extension.equals(FileTool.png))
                    return true;
            else
                return false;
        }
        return false;
    }
    /**
     * Questo metodo ritorna la descrizione di questo filtro.
     */
    public String getDescription() {
        return "Just Images";
    }
}