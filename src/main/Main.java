package main;

import java.awt.Color;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.time.LocalDate;

import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import graphics.MainFrame;
import photoAlbum.Album;

/**
 * Classe principale che permette il corretto avvio dell'applicazione.
 * @author Filippo Fontana
 * @version 1.0
 */
public class Main
{
	/**
	 * Inizializzazione del file che contiene la versione di ripristino dell'app.
	 */
	private static File backUpFile = new File("");
	private static String backUpFilePath = backUpFile.getAbsolutePath() + File.separator
										   + "res" + File.separator + "backUpAlbum.ser";
	
	public static void main(String[] args)
	{
		Album album = checkForBackUp();
		setLookAndFeel();
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new MainFrame(album, album.getName());
			}
		});
	}
	/**
	 * Questo metodo controlla se � presente una vecchia versione dell'app,
	 * se non ne viene trovata nessuna, viene creato un nuovo oggetto album.
	 * @return l'oggetto album.
	 */
	public static Album checkForBackUp()
	{
		backUpFile = new File(backUpFilePath);
		Album backUpAlbum = null;
		
		if (backUpFile.exists() && !backUpFile.isDirectory()) {
			ObjectInputStream ois;
			
			try {
				ois = new ObjectInputStream(new FileInputStream(backUpFilePath));
				backUpAlbum = (Album) ois.readObject();
				ois.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} 
		} else {
			backUpAlbum = createAlbum();
		}
		
		return backUpAlbum;
	}
	/**
	 * Questo metodo alloca un nuovo oggetto album con dei valori predefiniti.
	 * @return l'oggetto album.
	 */
	public static Album createAlbum()
	{
		String name = "Default Album";
		LocalDate creationDate = LocalDate.now();
		
		return new Album(name, creationDate);
	}
	/**
	 * Questo metodo imposta il corretto look and feel per i JDialogs.
	 */
	public static void setLookAndFeel()
	{
    	UIManager.put("OptionPane.background", Color.WHITE);
    	UIManager.put("Panel.background", Color.WHITE);
    	UIManager.put("Button.background", Color.WHITE);
	}
}